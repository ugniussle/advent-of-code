package main

import (
	"bufio"
	"log"
	"os"
)

func logError(message string, err error) {
	if err != nil {
		log.Fatal(message, ' ', err)
	}
}

func main() {
	file, err := os.Open("data")

	logError("error opening file ", err)

	defer file.Close()

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	var opponentSigns []string
	var playerSigns []string

	for fileScanner.Scan() {
		line := fileScanner.Text()

		opponentSign := line[0]
		playerSign := line[2]

		opponentSigns = append(opponentSigns, string(opponentSign))
		playerSigns = append(playerSigns, string(playerSign))
	}

	//
	// part 1
	//

	score := 0

	for i := 0; i < len(opponentSigns); i++ {
		score += determineMatchScorePartOne(playerSigns[i], opponentSigns[i])
	}

	println("Part 1: ", score)

	//
	// part 2
	//

	score = 0

	for i := 0; i < len(opponentSigns); i++ {
		score += determineMatchScorePartTwo(playerSigns[i], opponentSigns[i])
	}

	println("Part 2: ", score)
}

func determineMatchScorePartOne(player string, opponent string) (score int) {
	score = 0

	//
	// Rock - 		A and X
	// Paper - 		B and Y
	// Scissors - 	C and Z
	//

	switch player {
	case "X":
		score++
		break
	case "Y":
		score += 2
		break
	case "Z":
		score += 3
		break
	}

	if player == "X" && opponent == "A" ||
		player == "Y" && opponent == "B" ||
		player == "Z" && opponent == "C" {
		score += 3
	} else if player == "X" && opponent == "C" ||
		player == "Y" && opponent == "A" ||
		player == "Z" && opponent == "B" {
		score += 6
	}

	return score
}

func determineMatchScorePartTwo(action string, opponent string) (score int) {
	score = 0

	//
	// Rock - 		A
	// Paper - 		B
	// Scissors - 	C
	//
	// Should lose - X
	// Should draw - Y
	// Should win - Z
	//

	player := ""

	if action == "X" { // should lose
		switch opponent {
		case "A":
			player = "C"
			break
		case "B":
			player = "A"
			break
		case "C":
			player = "B"
			break
		}
	} else if action == "Y" { // should draw
		player = opponent
	} else if action == "Z" { // should win
		switch opponent {
		case "A":
			player = "B"
			break
		case "B":
			player = "C"
			break
		case "C":
			player = "A"
			break
		}
	}

	switch player {
	case "A":
		score++
		break
	case "B":
		score += 2
		break
	case "C":
		score += 3
		break
	}

	if player == "A" && opponent == "A" ||
		player == "B" && opponent == "B" ||
		player == "C" && opponent == "C" {
		score += 3
	} else if player == "A" && opponent == "C" ||
		player == "B" && opponent == "A" ||
		player == "C" && opponent == "B" {
		score += 6
	}

	return score
}
