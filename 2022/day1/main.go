package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
)

func logError(message string, err error) {
	if err != nil {
		log.Fatal(message, ' ', err)
	}
}

func main() {
	//
	// part 1
	//

	file, err := os.Open("./data")

	logError("error opening file", err)

	defer file.Close()

	var elfCount int = 0

	var elfCalorieCounts []int
	elfCalorieCounts = append(elfCalorieCounts, 0)

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		if fileScanner.Text() == "" {
			elfCalorieCounts = append(elfCalorieCounts, 0)
			elfCount++
		} else {
			calories, err := strconv.Atoi(fileScanner.Text())

			logError("error converting string to integer", err)

			elfCalorieCounts[elfCount] = elfCalorieCounts[elfCount] + calories
		}
	}

	var biggestCalorieCount int = 0

	for i := 0; i < elfCount; i++ {
		if elfCalorieCounts[i] > biggestCalorieCount {
			biggestCalorieCount = elfCalorieCounts[i]
		}
	}

	println("biggest calorie count: ", biggestCalorieCount)

	//
	// part 2
	//

	sort.Ints(elfCalorieCounts)

	topThreeCalorieCount := elfCalorieCounts[elfCount] + elfCalorieCounts[elfCount-1] + elfCalorieCounts[elfCount-2]

	println("top 3 calorie count: ", topThreeCalorieCount)
}
