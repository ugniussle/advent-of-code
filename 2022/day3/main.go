package main

import (
	"bufio"
	"log"
	"os"
)

func logError(message string, err error) {
	if err != nil {
		log.Fatal(message, ' ', err)
	}
}

func itemToPriority(item rune) (priority int) {
	if item > 64 && item < 91 {
		priority = int(item) - 64 + 26
	} else if item > 96 && item < 123 {
		priority = int(item) - 96
	} else {
		priority = 0
	}

	return
}

func arrayContainsRune(array []rune, x rune) bool {
	for _, value := range array {
		if value == x {
			return true
		}
	}

	return false
}

func getUsedItems(compartment []rune) (usedItems []rune) {
	for _, item := range compartment {
		if !arrayContainsRune(usedItems, item) {
			usedItems = append(usedItems, item)
		}
	}

	return
}

func getCompartments(rucksack []rune) (compartment1, compartment2 []rune) {
	length := len(rucksack)

	compartment1 = []rune(rucksack[0 : length/2])
	compartment2 = []rune(rucksack[length/2 : length])

	return
}

func main() {
	file, err := os.Open("data")

	logError("error opening file", err)

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	var rucksacks [][]rune

	for fileScanner.Scan() {
		rucksacks = append(rucksacks, []rune(fileScanner.Text()))
	}

	//
	// part 1
	//

	prioritySum := 0

	for _, rucksack := range rucksacks {
		compartment1, compartment2 := getCompartments(rucksack)

		usedItems1 := getUsedItems(compartment1)
		usedItems2 := getUsedItems(compartment2)

		for _, item := range usedItems1 {
			if arrayContainsRune(usedItems2, item) {
				prioritySum += itemToPriority(item)
			}
		}
	}

	println(prioritySum)

	//
	// part 2
	//

	prioritySum = 0

	for i := 0; i < len(rucksacks); i += 3 {
		rucksack1 := rucksacks[i]
		rucksack2 := rucksacks[i+1]
		rucksack3 := rucksacks[i+2]

		usedItems1 := getUsedItems([]rune(rucksack1))
		usedItems2 := getUsedItems([]rune(rucksack2))
		usedItems3 := getUsedItems([]rune(rucksack3))

		for _, item := range usedItems1 {
			if arrayContainsRune(usedItems2, item) {
				if arrayContainsRune(usedItems3, item) {
					prioritySum += itemToPriority(item)
				}
			}
		}
	}

	println(prioritySum)
}
