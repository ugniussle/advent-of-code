package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func logError(message string, err error) {
	if err != nil {
		log.Fatal(message, ' ', err)
	}
}

type assigmentPair struct {
	elf1low  int
	elf1high int
	elf2low  int
	elf2high int
}

func parseAssigmentString(line string) (pair assigmentPair) {
	var (
		temp  string
		count int
		err   error
	)

	for _, character := range line {
		if character > 47 && character < 58 {
			temp += string(character)
		} else {
			if count == 0 {
				pair.elf1low, err = strconv.Atoi(temp)
			} else if count == 1 {
				pair.elf1high, err = strconv.Atoi(temp)
			} else if count == 2 {
				pair.elf2low, err = strconv.Atoi(temp)
			}

			temp = ""

			count++
		}
	}

	pair.elf2high, err = strconv.Atoi(temp)

	logError("error converting string to int", err)

	return
}

func main() {
	file, err := os.Open("data")

	logError("error opening file", err)

	fileScanner := bufio.NewScanner(file)

	fileScanner.Split(bufio.ScanLines)

	var assigmentPairs []assigmentPair

	for fileScanner.Scan() {
		assigmentPairs = append(
			assigmentPairs,
			parseAssigmentString(fileScanner.Text()),
		)
	}

	//
	// part 1
	//

	var overlapCount int

	for _, pair := range assigmentPairs {
		if (pair.elf1low >= pair.elf2low && pair.elf1high <= pair.elf2high) ||
			(pair.elf1low <= pair.elf2low && pair.elf1high >= pair.elf2high) {
			overlapCount++
		}
	}

	println(overlapCount)

	//
	// part 2
	//

	overlapCount = 0

	for _, pair := range assigmentPairs {
		if pair.elf1high >= pair.elf2low && pair.elf1low <= pair.elf2low ||
			pair.elf2high >= pair.elf1low && pair.elf2low <= pair.elf1low {
			overlapCount++
		}
	}

	println(overlapCount)
}
