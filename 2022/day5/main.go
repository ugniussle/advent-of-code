package main

import (
	"bufio"
	"errors"
	"log"
	"os"
	"strconv"
)

func logError(message string, err error) {
	if err != nil {
		log.Fatal(message, ' ', err)
	}
}

// Stack (LIFO)
type stack struct {
	stack []rune
}

func newStack() *stack {
	return &stack{make([]rune, 0)}
}

func (s *stack) push(r rune) {
	s.stack = append(s.stack, r)
}

func (s *stack) pop() (rune, error) {
	length := len(s.stack)

	if length == 0 {
		return 0, errors.New("Empty Stack")
	}

	r := s.stack[length-1]
	s.stack = s.stack[:length-1]

	return r, nil
}

type instruction struct {
	amount int
	from   int
	to     int
}

func getPileCount(line string) int {
	return (len(line) + 1) / 4
}

func parseInstructionString(line string) (instruction instruction) {
	var (
		temp  string
		count int
		err   error
	)

	for _, character := range line {
		if character > 48 && character < 58 {
			temp += string(character)
		} else if temp != "" {
			if count == 0 {
				instruction.amount, err = strconv.Atoi(temp)
			} else if count == 1 {
				instruction.from, err = strconv.Atoi(temp)
			}

			temp = ""
			count++
		}
	}

	instruction.to, err = strconv.Atoi(temp)

	logError("error converting string to int", err)

	return
}

func main() {
	file, err := os.Open("data.txt")
	defer file.Close()

	logError("error opening file", err)

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	var lines []string = make([]string, 0)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if len(line) > 0 && rune(line[1]) == '1' {
			break
		} else {
			lines = append(lines, line)
		}
	}

	pileCount := getPileCount(lines[0]) + 1

	var piles []*stack

	for i := 0; i < pileCount; i++ {
		piles = append(piles, newStack())
	}

	for i, j := 0, len(lines)-1; i < j; i, j = i+1, j-1 {
		lines[i], lines[j] = lines[j], lines[i]
	}

	for _, line := range lines {
		var pile int = 1

		for i := 1; i < len(line); i += 4 {
			r := rune(line[i])

			if r > 64 && r < 91 {
				piles[pile].push(r)
			}

			pile++
		}
	}

	//
	// part 1
	//

	instructions := make([]instruction, 0)

	fileScanner.Scan()

	for fileScanner.Scan() {
		line := fileScanner.Text()

		instructions = append(instructions, parseInstructionString(line))
	}

	//reader := bufio.NewReader(os.Stdin)

	for _, inst := range instructions {

		if inst.from == 9 || inst.to == 9 {
			println("from", inst.from, string(piles[inst.from].stack))
			println("to", inst.to, string(piles[inst.to].stack))
			println("moving", inst.amount, "times")
			println()
		}

		for i := 0; i < inst.amount; i++ {
			var crate rune
			crate, err = piles[inst.from].pop()

			piles[inst.to].push(crate)

			//println("from", string(piles[inst.from].stack))
			//println("to", string(piles[inst.to].stack))
		}

		//for i := 0; i < len(piles); i++ {
		//	println(i, string(piles[i].stack))
		//}

		//println()

		//reader.ReadString('\n')
	}

	logError("Stack error", err)

	for i := 0; i < len(piles); i++ {
		println(i, string(piles[i].stack))
	}
}
