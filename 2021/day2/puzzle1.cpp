#include <iostream>
#include <fstream>
#include <vector>
struct Movement{
    std::string direction;
    int distance;
};
int main(){
    std::vector<Movement> data;
    std::ifstream file("puzzle1.txt");
    while(!file.eof()){
        std::string direction;
        int distance;
        file>>direction>>distance;
        data.push_back({direction,distance});
    }
    //for(int i=0;i<data.size();i++)std::cout<<data[i].direction<<" "<<data[i].distance<<std::endl;
    int horizontal=0,vertical=0;
    for(int i=0;i<data.size();i++){
        if(data[i].direction=="forward") horizontal+=data[i].distance;
        if(data[i].direction=="down") vertical+=data[i].distance;
        if(data[i].direction=="up") vertical-=data[i].distance;
        else{}
    }
    std::cout<<vertical*horizontal;
}
