#include <iostream>
#include <fstream>
#include <vector>
int main(){
    std::ifstream file("puzzle1.txt");
    std::vector <int> data;
    while(!file.eof()){
        int temp;
        file>>temp;
        data.push_back(temp);
    }
    int previous=data[0];
    int count=0;
    for(int i=1;i<data.size();i++){
        if(data[i]>previous)count++;
        previous=data[i];
    }   
    std::cout<<count;
}