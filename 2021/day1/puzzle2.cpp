#include <iostream>
#include <fstream>
#include <vector>
int main(){
    std::ifstream file("puzzle1.txt");
    std::vector <int> data;
    while(!file.eof()){
        int temp;
        file>>temp;
        data.push_back(temp);
    }
    int previousWindow=data[0]+data[1]+data[2];
    int count=0;
    for(int i=1;i<data.size();i++){
        int window = data[i]+data[i+1]+data[i+2];
        if(window>previousWindow)count++;
        previousWindow=window;
    }   
    std::cout<<count;
}